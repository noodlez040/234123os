#include <asm/uaccess.h> // for copy_from/to_user
#include <linux/slab.h> // for kmalloc
#include <linux/string.h> // for strcmp
#include <linux/sched.h>
#include <linux/blocklist.h>
#include <linux/accesslog.h> //for sys_get_forbidden_tries

#include <linux/kernel.h>

int sys_block_program(const char *name, unsigned int name_len) {
	if (!name || !name_len) {
		return -EINVAL;
	}
	if(blockListContains(name)) {
		return 1;
	}
	int res = blockListAdd(name, name_len);
	return res;
	// return 0;
}
 
int sys_unblock_program(const char *name, unsigned int name_len) {
	if(name == NULL || name_len == 0) {
		return -EINVAL;
	}
	if(!blockListContains(name)) { //was not blocked
		return 1;
	}
	int res = blockListDelete(name); //blockedListDelete returns 0 if deleted.
	return res;
	// return 0;
}
//in c false is represented as 0. should we check if name_len is negative?

int sys_is_program_blocked(const char *name, unsigned int name_len) {
	if(name == NULL || name_len == 0) {
		return -EINVAL;
	}
	int res = blockListContains(name);
	if(res != 0) { //a truthy val in c is non zero (can get pos or neg val)
		return 1;
	} else {
		return 0;
	}
	// return 0;
}

int sys_get_blocked_count(void) {
	return getBlockCount(); //check that adding and subtracting to counter was done right.
	// return 0;
}

int sys_get_forbidden_tries(int pid, char log[][256], unsigned int n) {
	if(n <= 0) {
		return -EINVAL;
	}

	task_t* p = find_task_by_pid(pid);
	if(!p) {
		return -ESRCH;
	}

	return getForbiddenTries(p, log, n);
	// return 0;
}
