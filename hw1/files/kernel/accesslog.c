#include <linux/list.h> // for list
#include <linux/slab.h> // for dynamic allocations (kmalloc, kfree, ...)
#include <asm/uaccess.h>// for copy_from/to_user
#include <linux/sched.h> // for task_t
#include <linux/string.h>
#include <linux/accesslog.h>

accessLogNode_t *createAccessNode(const char* name) {
	accessLogNode_t *newNode = kmalloc(sizeof(accessLogNode_t), GFP_KERNEL);
	// +1 for null terminator
	if(!newNode) {
		kfree(newNode);
		return NULL;
	}

	strcpy(newNode->fname, name);
	newNode->listRecord.next = &(newNode->listRecord);
	newNode->listRecord.prev = &(newNode->listRecord);
	return newNode;
}

void accessNodeDestroy(accessLogNode_t* nodeToKill) {
	kfree(nodeToKill);
}

void accessLogDestroy(struct task_struct *p) {
	list_t *listIter;
	list_t *accessLog = &(p->accessLog);
	list_for_each(listIter, accessLog) {
		list_del(listIter);
		accessNodeDestroy(list_entry(listIter, accessLogNode_t, listRecord));
	}
}

int accessLogAdd(const char* name) {
	accessLogNode_t *newNode = createAccessNode(name);
	if(!newNode) {
		return -ENOMEM;
	}
	list_add_tail(&(newNode->listRecord), &(current->accessLog));
	return 0;
}

int getForbiddenTries(struct task_struct *p, char log[][256], unsigned int n) {
	int counter = 0;
	list_t *listIter;
	list_t *accessLog = &(p->accessLog);
	list_for_each(listIter, accessLog) {
		accessLogNode_t *src = list_entry(listIter, accessLogNode_t, listRecord);
		int size = strlen(src->fname);
		if(copy_to_user(log[counter++], src, size + 1) > 0) {
			// not all data copied, might be accessing inaccessible memory
			return -EFAULT;
		}
		if(counter == n) {
			break;
		}
	}
	return counter;
}
