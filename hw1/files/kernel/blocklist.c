#include <linux/list.h> // for list
#include <linux/slab.h> // for dynamic allocations (kmalloc, kfree, ...)
#include <asm/uaccess.h> // for copy_from/to_user
#include <linux/blocklist.h>

// blockList is a singleton!
// don't understand this
LIST_HEAD(blockList);
int blockCounter = 0;

blockNode_t *createBlockNode(const char* name, unsigned int name_len, int *error) { //GFP_KERNEL is a flag to allocate memory in Kernel 
	blockNode_t *newNode = kmalloc(sizeof(blockNode_t), GFP_KERNEL);
	if (!newNode) {
		kfree(newNode);
		*error = -ENOMEM;
		return NULL;
	}
	// +1 for null terminator
	if(copy_from_user(newNode->pname, name, name_len + 1) > 0) { //copies memory from user to kernel, returns memory it could not copy
		// couldn't copy all data
		kfree(newNode);
		*error = -EINVAL;
		return NULL;
	}
	newNode->listRecord.next = &(newNode->listRecord);
	newNode->listRecord.prev = &(newNode->listRecord);

	*error = 0;
	return newNode;
}

void deleteNode(blockNode_t *node) {
	kfree(node);
}

list_t *getBlockList(void) { //returns address
	return &blockList;
}

int blockListContains(const char* name) {
	return blockListFind(name) != NULL;
}

blockNode_t* blockListFind(const char* name) {
	list_t *listIter;
	list_for_each(listIter, &blockList) {
		blockNode_t *node = list_entry(listIter, blockNode_t, listRecord);
		if (strcmp(node->pname, name) == 0) {
			return node;
		}
	}
	return NULL;
}

int blockListAdd(const char* name, unsigned int name_len) {
	int error = 0;
	blockNode_t *newNode = createBlockNode(name, name_len, &error);
	if (!newNode) {
		return error;
	}
	list_add(&(newNode->listRecord), &blockList);
	blockCounter++;
	return 0;
}

int blockListDelete(const char* name) {
	blockNode_t *node = blockListFind(name);
	if (node) {
		list_del(&(node->listRecord));
		deleteNode(node);
		blockCounter--;
		return 0;
	}
	return 1;
}

int getBlockCount(void) {
	return blockCounter;
}
