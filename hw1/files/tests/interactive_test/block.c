#include "../../blocker.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	char cmd[10];
	char path[256];
	int res = -1000;

	strcpy(cmd, argv[1]);

	if (strcmp(cmd, "block") == 0)
	{
		strcpy(path, argv[2]);
		res = block_program(path, strlen(path));
	}
	else if (strcmp(cmd, "unblock") == 0)
	{
		strcpy(path, argv[2]);
		res = unblock_program(path, strlen(path));
	}
	else if (strcmp(cmd, "isblock") == 0)
	{
		strcpy(path, argv[2]);
		res = is_program_blocked(path, strlen(path));
	}
	else if (strcmp(cmd, "count") == 0)
	{
		res = get_blocked_count();
	}
	else if (strcmp(cmd, "get") == 0)
	{
		char log[1000][256];
		int pid = atoi(argv[2]);
		int i;
		unsigned int n = (unsigned int) atoi(argv[3]);
		res = get_forbidden_tries(pid, log, n);
		for (i = 0; i < res; i++)
		{
			printf("log[%d]=%s\n", i, log[i]);
		}
	}
	else if (strcmp(cmd, "run") == 0)
	{
		printf("process pid: %d\n", getpid());
		while (1)
		{
			printf("enter path: ");
			scanf("%s", cmd);
			execve(cmd, NULL, NULL);
		}
	}
	printf("res:%d\n", res);

	return 0;
}
