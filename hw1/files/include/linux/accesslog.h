#ifndef _ACCESS_LOG
#define _ACCESS_LOG

#include <linux/sched.h>
#include <linux/list.h>

typedef struct accessLogNode {
	char fname[256]; // filename as given to block_program
	list_t listRecord; //check the implementation in linux/list.h
} accessLogNode_t;

/*
if i didn't write accessLogNode_t i would be able to access
accessLogNode as struct accessLogNode.
*/

int accessLogAdd(const char* name); //returns 1 on success, 0 on failure
void accessNodeDestroy(accessLogNode_t* nodeToKill);
void accessLogDestroy(struct task_struct *p);
int getForbiddenTries(struct task_struct *p, char log[][256], unsigned int n);
#endif
