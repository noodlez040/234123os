#ifndef _BLOCK_LIST
#define _BLOCK_LIST

#include <linux/list.h>

typedef struct blockNode {
	char pname[256]; // process name
	list_t listRecord;
} blockNode_t;

list_t *getBlockList(void);
int blockListContains(const char* name);
blockNode_t* blockListFind(const char* name);
int blockListAdd(const char* name, unsigned int name_len);
int blockListDelete(const char* name);
int getBlockCount(void);

#endif
