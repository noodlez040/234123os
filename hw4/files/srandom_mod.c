#include <linux/module.h>
#include <linux/fs.h>
#include <linux/random.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/capability.h>
#include <linux/wait.h>
#include <asm/uaccess.h>
#include <linux/slab.h> // kmalloc

#define POOL_SIZE 512
#define CHUNK_SIZE 64
#define MIN_ENT 8
#define HASH_SIZE 20

MODULE_LICENSE("GPL");

typedef struct EntPool {
  char data[POOL_SIZE];
  int count;
} pool_t;


int open_rand (struct inode* stam1, struct file* stam2);
int release (struct inode* stam1, struct file* stam2);
int flush (struct file* stam1);
loff_t llseek (struct file* stam1, loff_t stam2, int stam3);
ssize_t read(struct file* filp, char* buff, size_t count, loff_t* offp);
ssize_t write(struct file* filp, const char* buff, size_t count, loff_t* offp);
int ioctl(struct inode* inode, struct file* filp, unsigned int cmd, unsigned long arg);

pool_t ent_pool;
DECLARE_WAIT_QUEUE_HEAD(queue);
struct file_operations fops = {
  .open = open_rand,
  .release = release,
  .flush = flush,
  .read = read,
  .write = write,
  .llseek = llseek,
  .ioctl = ioctl
};

int init_module(void) {
  SET_MODULE_OWNER(&fops);
  memset(&(ent_pool.data), 0, POOL_SIZE);
  ent_pool.count = 0;
  int register_status = register_chrdev(62, "srandom", &fops);
  if (register_status < 0) {
    return register_status;
  }

  return 0;
}

void cleanup_module(void) {
  unregister_chrdev(62, "srandom");
}

ssize_t read(struct file* filp, char* buff, size_t count, loff_t* offp) {
  char* tmp[HASH_SIZE] = { 0 };
  int i;
  if (count == 0) {
    return count;
  }
  if (wait_event_interruptible(queue, ent_pool.count >= MIN_ENT) < 0) {
    return -ERESTARTSYS;
  }
  count = count > (ent_pool.count / 8) ? ent_pool.count / 8 : count;

  for (i = 0; i < count; i += HASH_SIZE) {
    hash_pool(&(ent_pool.data), tmp);
    mix(tmp, HASH_SIZE, &(ent_pool.data));
    int copy_amount = i + HASH_SIZE <= count ? HASH_SIZE : count - i;
    if (copy_to_user(buff + i, tmp, copy_amount) != 0) {
      return -EFAULT;
    }
  }
  ent_pool.count -= count * 8;

  return count;
}

ssize_t write(struct file* filp, const char* buff, size_t count, loff_t* offp) {
  char tmp_buff[count];
  int i;
  int copy_res = copy_from_user(tmp_buff, buff, count);
  // printk("HW4 write tmp_buff %p to copy %d copy_res %d copied %d\n", &tmp_buff, count, copy_res, count - copy_res);
  if (copy_res) {
    return -EFAULT;
  }
  for (i = 0; i < count; i += CHUNK_SIZE) {
    int copy_amount = i + CHUNK_SIZE <= count ? CHUNK_SIZE : count - i;
    mix(tmp_buff + i, (size_t) copy_amount, &(ent_pool.data));
  }

  return count;
}

int ioctl(struct inode* inode, struct file* filp, unsigned int cmd, unsigned long arg) {
  switch (cmd) {
    case RNDGETENTCNT:
      return copy_to_user((int*) arg, &(ent_pool.count), sizeof(int)) == 0 ? 0 : -EFAULT;
    case RNDCLEARPOOL:
      if (!capable(CAP_SYS_ADMIN)) {
        return -EPERM;
      }
      ent_pool.count = 0;
      return 0;
    case RNDADDENTROPY:
      if (!capable(CAP_SYS_ADMIN)) {
        return -EPERM;
      }
      struct rand_pool_info* p;
      if (copy_from_user(&p, (struct rand_pool_info*) arg, sizeof(struct rand_pool_info)) != 0) {
        // dummy code to test if the pointer in arg is accessible
        return -EFAULT;
      }
      p = (struct rand_pool_info*) arg;
      if (p->entropy_count < 0) {
        return -EINVAL;
      }
      int result = write(filp, (char*) p->buf, p->buf_size, NULL);
      if (result < 0) {
        return result;
      } else {
        ent_pool.count += p->entropy_count;
        ent_pool.count = ent_pool.count > (POOL_SIZE * 8) ? POOL_SIZE * 8 : ent_pool.count;
        wake_up_interruptible(&queue);
        return 0;
      }
  }
  return -EINVAL;
}

int open_rand (struct inode* stam1, struct file* stam2) {
  return 0;
}
int release (struct inode* stam1, struct file* stam2) {
  return 0;
}
int flush (struct file* stam1) {
  return 0;
}
loff_t llseek (struct file* stam1, loff_t stam2, int stam3) {
  return 0;
}
