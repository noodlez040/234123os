echo "Kernel compilation begins."
cd /usr/src/linux-2.4.18-14custom
echo "Running make bzImage command"
make bzImage
echo "Running make modules command"
make modules
echo "Running make modules_install"
make modules_install
echo "Opening arch/i386/boot directory"
cd arch/i386/boot
echo "Invoking cp bzImage /boot/vmlinuz-2.4.18-14custom"
cp bzImage /boot/vmlinuz-2.4.18-14custom
echo "Accessing /boot"
cd /boot
echo "invoking mkinitrd 2.4.18-14custom.img 2.4.18-14custom"
mkinitrd 2.4.18-14custom.img 2.4.18-14custom
echo "Reboot"
reboot
