#!/bin/bash

# User specific aliases and functions

okernel='/usr/src/linux-2.4.18-14'
ckernel='/usr/src/linux-2.4.18-14custom'
shared='/mnt/hgfs/shared'
shared0='/mnt/hgfs/shared/hw0/files'
shared1='/mnt/hgfs/shared/hw1/files'
shared2='/mnt/hgfs/shared/hw2/files'
shared3='/mnt/hgfs/shared/hw3/files'
shared4='/mnt/hgfs/shared/hw4/files'
sharedArr=(\
	${shared0}\
	${shared1}\
	${shared2}\
	${shared3}\
	${shared4}\
	)

# alias rm='rm -i'
# alias cp='cp -i'
# alias mv='mv -i'
alias cpbashrc='cp -f /mnt/hgfs/shared/utils/.bashrc ~/.bashrc && source ~/.bashrc'
alias cdcustom='cd /usr/src/linux-2.4.18-14custom'
alias compker='bash /mnt/hgfs/shared/utils/compker'
alias copy2ker='bash /mnt/hgfs/shared/utils/copy2ker'
alias copycompile='bash /mnt/hgfs/shared/utils/copycompile'

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Does diff between original kernel and custom kernel / file in shared folder
# $1 - name of file to diff. relative to '/usr/src/linux-2.4.18-14custom'
# $2 (Optional) - number of HW to search file in.
#	If given, will search in /mnt/hgfs/shared/hw[$2]...
#	If not, will search in '/usr/src/linux-2.4.18-14custom'
function diffk() {
	file="$1"
	if [[ ${file[0]} != "/" ]]; then
		file="/${file}"
	fi
	if [ -z $2 ]; then
		echo "Comparing Kernel-Custom to Kernel"
		diff ${ckernel}${file} ${okernel}${file}
	else
		echo "Comparing edited in Shared to Kernel"
		diff ${sharedArr[$2]}${file} ${okernel}${file}
	fi
}

# Copies file from kernel to shared folder of relevant HW.
# Path must be relative to '/usr/src/linux-2.4.18-14custom'
# cpk [OPTION]... [FILE] [HW #]
# -o Copy from original kernel, not custom
function cpk() {
	original=false
	args=()
	for arg in "$@"; do
		case ${arg} in
			"-o") original=true
			;;
			*) args=(${args} ${arg})
		esac
	done
	if [ -z ${args[1]} ]; then
		echo "No HW # passed."
		return
	fi
	file="${args[0]}"
	if [[ ${file[0]} != "/" ]]; then
		file="/${file}"
	fi
	if [[ "$original" = "true" ]]; then
		echo "Copying from original kernel"
		cp ${okernel}${file} ${sharedArr[${args[1]}]}${file}
	else
		echo "Copying from custom kernel"
		cp ${ckernel}${file} ${sharedArr[${args[1]}]}${file}
	fi
}
