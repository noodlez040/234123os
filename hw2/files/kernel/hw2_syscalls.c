#include <asm/param.h>	/* for HZ */
#include <linux/sched.h>
// #include <linux/kernel.h>

int sys_is_SHORT(int pid) {
	task_t *p = find_task_by_pid(pid);
	if (!p || !task_short(p)) {
		return -EINVAL;
	} else {
		if (task_short_overdue(p)) {
			return 0;
		} else {
			return 1;
		}
	}
}
 
int sys_remaining_time(int pid) {
	task_t *p = find_task_by_pid(pid);
	if (!p || !task_short(p)) {
		return -EINVAL;
	} else if (task_short_overdue_perm(p)) {
		return 0;
	} else {
		return p->time_slice * 1000 / HZ;
	}
}

int sys_remaining_cooloffs(int pid) {
	task_t *p = find_task_by_pid(pid);
	if (!p || !task_short(p)) {
		return -EINVAL;
	} else {
		return p->number_of_cooloffs_left;
	}
}
