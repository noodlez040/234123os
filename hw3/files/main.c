#include <pthread.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

using namespace std;

class NumberNode {
private:
	pthread_mutex_t mutex;
	NumberNode();
public:
	NumberNode* _next;
	NumberNode* _prev;
	~NumberNode() {
		this->unlock();
		pthread_mutex_destroy(&mutex);
	}
	int value;
	NumberNode(int value) : value(value), _next(NULL), _prev(NULL) {
		pthread_mutex_init(&mutex, NULL);
	}
	int lock() {
		return pthread_mutex_lock(&mutex);
	}
	int unlock() {
		return pthread_mutex_unlock(&mutex);
	}
	NumberNode* next() {
		NumberNode* next = this->_next;
		if (next) {
			next->lock();
		}
		this->unlock();
		return next;
	}
	/*
	 * Assumption: Current and Next nodes' mutexes are owned by current thread.
	 */
	void removeNext() {
		NumberNode* next = this->_next;
		NumberNode* nextnext = this->_next->_next;
		this->_next = nextnext;
		if (nextnext) {
			nextnext->lock();
			nextnext->_prev = this;
			nextnext->unlock();
			delete next;
		} else {
			// node after the one being removed is NULL.
			delete next;
		}
	}
};

class NumbersList {
private:
	NumberNode* _head;
	NumbersList();
public:
	NumbersList(int N) {
		_head = new NumberNode(2);
		NumberNode* prev = _head;
		for(int i = 3; i <= N; i++) {
			NumberNode* node = new NumberNode(i);
			node->_prev = prev;
			prev->_next = node;
			prev = node;
		}
	}
	virtual ~NumbersList() {
		NumberNode* current = _head;
		NumberNode* next = _head;
		while (next) {
			current = next;
			next = current->_next;
			delete current;
		}
	}
	NumberNode* head() {
		return _head;
	}
};

pthread_mutex_t threadNumMutex;
int N, T;

void* findPrimes(void* vlist) {
	static int threadNum = 0;
	pthread_mutex_lock(&threadNumMutex);
	// number the threads without concurrency problem.
	int currentThread = ++threadNum;
	pthread_mutex_unlock(&threadNumMutex);
	NumbersList* list = (NumbersList*) vlist;
	ostringstream filename;
	filename << "thread-" << currentThread << ".log";
	ofstream threadlog;
	threadlog.open(filename.str().c_str(), ios::out | ios::trunc);

	/* HANDLE PRIMARY NUMBERS */
	NumberNode* candidate = ((NumbersList*) list)->head();
	candidate->lock();
	while (candidate) {
		int candidateVal = candidate->value;
		if (candidateVal * candidateVal > N) {
			candidate->unlock();
			break;
		}
		NumberNode* current = candidate;
		NumberNode* next = candidate->_next; // check if next exists?
		next->lock();
		/* the first element to be removed will be candidate^2
		 * because if we take for example 6 = 2 * 3, then when going
		 * over 3, 2 will already have removed it.
		 */
		int stopFlag = 0;
		while (next && next->value < candidateVal * candidateVal) {
			current->unlock();
			current = next;
			next = next->_next;
			if (next) {
				next->lock();
			} else {
				stopFlag = 1;
			}
		}
		if (stopFlag) {
			/* stop flag means that we are checking a prime candidate for which
			 * p^2 does not exist, meaning we can finish our work.
			 */
			 current->unlock();
			 break;
		}
		if (next && next->value == candidateVal * candidateVal) {
			threadlog << "prime " << candidateVal << endl;
			/* the candidate hasn't been checked before!
			 * we can go on to remove all numbers related to it.
			 */
			while (current && next) {
				if (next->value % candidateVal == 0) {
					threadlog << next->value << endl;
					current->removeNext();
					// don't change current, cause we have a new "next" node
					next = current->_next;
				} else {
					current->unlock();
					current = next;
					next = next->_next;
				}
				if (next) {
					next->lock();
				}
			}
			current->unlock();

		} else {
			// the prime candidate is already being handled. move on to next one
			current->unlock();
			next->unlock();
		}
		NumberNode* tmp = candidate;
		tmp->lock();
		candidate = candidate->_next;
		if (candidate) {
			candidate->lock();
		}
		tmp->unlock();
	}
	threadlog.close();
	pthread_exit(NULL);
}

int main (int argc, char** argv) {
	N = atoi(argv[1]);
	T = atoi(argv[2]);
	pthread_mutex_init(&threadNumMutex, NULL);
	pthread_t* threads = new pthread_t[N];

	NumbersList* list = new NumbersList(N);

	for (int i = 0; i < T; i++) {
		pthread_create(threads + i, NULL, findPrimes, list);
	}
	for (int i = 0; i < T; i++) {
		pthread_join(threads[i], NULL);
	}
	ofstream primes;
	primes.open("primes.log", ios::out | ios::trunc);
	for (NumberNode* node = list->head(); node; node = node->_next) {
		primes << node->value << endl;
	}
	primes.close();
	delete list;
}