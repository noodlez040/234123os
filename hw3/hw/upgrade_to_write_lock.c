// global variables
bool changed_while_upgraded = false;
bool upgraded = false;

bool upgrade_to_write_lock() {
	pthread_mutex_lock(&global_lock);
	/* decrease number of readers, so that even if upgrade fails,
	 * thread is no longer considered reader.
	 */
	number_of_readers--;
	if (upgraded) {
		// if there already was a request to upgrade, then fail the operation
		pthread_mutex_unlock(&global_lock);
		return false;
	}
	upgraded = true;
	while(number_of_writers > 0 || number_of_readers > 0) {
		pthread_cond_wait(&writers_condition, &global_lock);
	}
	if (changed_while_upgraded) {
		// data structure changed while we waited for upgrade, fail the operation
		changed_while_upgraded = false;
		upgraded = false;
		pthread_mutex_unlock(&global_lock);
		return false;
	}
	number_of_writers++;
	pthread_mutex_unlock(&global_lock);
	return true;
}

void write_unlock() {
	pthread_mutex_lock(&global_lock);
	number_of_writers--;
	if (number_of_writers == 0) {
		pthread_cond_broadcast(&readers_condition);
		pthread_cond_signal(&writers_condition);
	}
	/* if there was an upgrade request while the current thread
	 * was writing (changing the data structure), then upgrade
	 * should fail. mark changed_while_upgraded to true so that
	 * upgrading thread will know this after cond_wait.
	 */
	if (upgraded) {
		changed_while_upgraded = true;
	}
	/* mark upgraded = false to let new threads try to upgrade
	 * in the next wave of readers.
	 */
	upgraded = false;
	pthread_mutex_unlock(&global_lock);
	// end of function
}
